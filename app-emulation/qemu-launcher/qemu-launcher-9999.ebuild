# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

inherit git-r3

DESCRIPTION="Simple QEMU wrapper"
HOMEPAGE="https://bitbucket.org/wuri/${PN}"
EGIT_REPO_URI="https://bitbucket.org/wuri/${PN}.git"
LICENSE=""
SLOT="0"
IUSE="+pci-bind"
RDEPEND=">=app-emulation/qemu-2.8.0"

src_install() {
	if use pci-bind ; then
		dobin pci-bind
	fi
	dobin qemu-launcher
}
