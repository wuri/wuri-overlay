# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A utility for mapping events from Linux event devices."
HOMEPAGE="https://github.com/KarsMulder/evsieve"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="virtual/rust"

CRATES="
	libc-0.2.82
	lazy_static-1.4.0
	signal-hook-0.3.3
	signal-hook-registry-1.3.0
"

inherit cargo

SRC_URI="https://github.com/KarsMulder/evsieve/archive/refs/tags/v${PV}.zip $(cargo_crate_uris)"

src_compile() {
	cargo_src_compile
}

src_install() {
	cargo_src_install
}
