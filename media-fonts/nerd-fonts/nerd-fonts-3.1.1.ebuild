# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit font

IUSE="-hack +iosevka -jetbrains-mono +ubuntu +ubuntu-mono"
RELEASE_URI="https://github.com/ryanoasis/nerd-fonts/releases/download/v${PV}/"

DESCRIPTION="Iconic font collection"
HOMEPAGE="https://www.nerdfonts.com/"
SRC_URI="
	hack? ( ${RELEASE_URI}Hack.zip )
	iosevka? ( ${RELEASE_URI}Iosevka.zip )
	jetbrains-mono? ( ${RELEASE_URI}JetBrainsMono.zip )
	ubuntu? ( ${RELEASE_URI}Ubuntu.zip )
	ubuntu-mono? ( ${RELEASE_URI}UbuntuMono.zip )
"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="app-arch/unzip"

S="${WORKDIR}"
FONT_SUFFIX="ttf"
