# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

inherit git-r3

DESCRIPTION="Allows setting monitor VCP controls using DDC/CI over i2c"
HOMEPAGE="https://bitbucket.org/wuri/${PN}"
EGIT_REPO_URI="https://bitbucket.org/wuri/${PN}.git"
LICENSE=""
SLOT="0"
IUSE="+suid"

src_compile() {
	emake
}

src_install() {
	dobin ${PN}
	if use suid; then
		fperms u+s /usr/bin/${PN}
	fi
}
