# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3

DESCRIPTION="Simple text-only statusbar"
HOMEPAGE="https://bitbucket.org/wuri/dwm-bar.git"
EGIT_REPO_URI="https://bitbucket.org/wuri/dwm-bar.git"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND=">=x11-libs/libX11-1.8.7"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	dobin dwm-bar
}
